(function( $ ) { 
/**
 * START - ONLOAD - JS
 * 1. Section Slider
 * 2. Agence Slider
 * 3. Show Menu Right
 * 4. Close Menu Right
 * 5. Show Menu Profile
 * 6. Show Password
 * 7. Show Search Voice
 * 8. Close Search Voice
 * 9. Fiche slider
 * 10. Derniers slider
 * 11. Init Map
 * 12. Show Buttom More page recherche-affiner
 * 13. Home Agences slider
 * 14. Change Scroll Text
 * 15. Scroll down block home desktop
 * 16. Show Filter recherche desktop
 * 17. Hide Filter recherche desktop
 */
/* ----------------------------------------------- */
/* ------------- FrontEnd Functions -------------- */
/* ----------------------------------------------- */
/**
 * 1. Section Slider
 */
function sectionSlider() {
    if(!$(".home-section-slider").length) { return; }

    $('.home-section-slider').slick({
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        arrows: true,
        dots: true
    });
}
/**
 * 2. Agence Slider
 */
function agenceSlider() {
    if(!$('.agence-list').length) { return; }

    $('.agence-list').slick({
        dots: true,
        infinite: true,
        speed: 500,
        arrows: false,
        slidesToShow: 1,
        centerMode: true,
        variableWidth: true
    });
}
/**
 * 3. Show Menu Right
 */
function showMenuRight(objMenu) {
    if(!$(".menu-popup").length) { return; }

    $(objMenu).on('click', function(e) {
        e.preventDefault();

        var $a_menu     =   $(this),
            $body       =   $a_menu.closest('body'),
            $menu       =   $body.find('.menu-popup');
        
        $body.addClass('mn-opened');
        $menu.addClass('shw');
    });

    // click out
    $(document).on('click', function(e) {
        if($(e.target).is('.recherche-menu')
        || $(e.target).is('.recherche-menu *')
        || $(e.target).is('.menu-popup')
        || $(e.target).is('.menu-popup *')
        || $(e.target).is('.menu_click')
        || $(e.target).is('.menu_click *')) { return;}
        
        $('.menu-popup').removeClass('shw');
        $('body').removeClass('mn-opened');
    })
}
/**
 *  4. Close Menu Right
 */
function closeMenuRight() {
    if(!$(".menu-popup").length) { return; }

    $('.close_menu').on('click', function(e) {
        e.preventDefault();

        var $a_close    =   $(this),
            $body       =   $a_close.closest('body'),
            $menu       =   $a_close.closest('.menu-popup');
        
        $menu.removeClass('shw');
        $body.removeClass('mn-opened');
    });
}
/**
 * 5. Show Menu Profile
 */
function showMenuProfile() {
    if(!$(".link_profile.login").length) { return; }

    $(".link_profile.login").on('click', function(e) {
        e.preventDefault();
        var $a_menu     =   $(this),
            $menu       =   $a_menu.siblings('.profile-menu');
        
        if($a_menu.hasClass('act')) {
            $a_menu.removeClass('act');
            $menu.removeClass('shw');
        } else {
            $a_menu.addClass('act');
            $menu.addClass('shw');
        }
    });
}
/**
 * 6. Show Password
 */
function showPassword() {
    if(!$(".link_pass").length) { return; }

    $(".link_pass").on('click', function(e) {
        e.preventDefault();

        var $a_click    =   $(this),
            $ipt        =   $a_click.siblings('.ipt');

        if($a_click.hasClass('act')) {
            $a_click.removeClass('act');
            $ipt.attr('type', 'password');
        } else {
            $a_click.addClass('act');
            $ipt.attr('type', 'text');
        }
    });
}
/**
 * 7. Show Search Voice
 */
function showSearchVoice() {
    if(!$('.action_voice').length) { return; }

    $('.action_voice').on('click', function(e) {
        e.preventDefault();

        var $a_voice    =   $(this),
            $body       =   $a_voice.closest('body'),
            $search     =   $body.find('.search-voice-wrap');

        if($a_voice.hasClass('act')) {
            $a_voice.removeClass('act');
            $body.removeClass('blocked');
            $search.removeClass('shw');
        } else {
            $a_voice.addClass('act');
            $body.addClass('blocked');
            $search.addClass('shw');
        }
    });

    // click out
    $(document).on('click', function(e) {
        if($(e.target).is('.action_voice')
        || $(e.target).is('.action_voice *')
        || $(e.target).is('.search-voice-wrap')
        || $(e.target).is('.search-voice-wrap *')) { return;}
        
        $('.action_voice').removeClass('act');
        $('body').removeClass('blocked');
        $('.search-voice-wrap').removeClass('shw');
    })
}
/**
 * 8. Close Search Voice
 */
function closeSearchVoice() {
    if(!$('.close_voide').length) { return; }

    $('.close_voide').on('click', function(e) {
        e.preventDefault();

        var $a_close    =   $(this),
            $body       =   $a_close.closest('body'),
            $a_voice    =   $body.find('.action_voice'),
            $search     =   $a_close.closest('.search-voice-wrap');

        $a_voice.removeClass('act');
        $body.removeClass('blocked');
        $search.removeClass('shw');
    });
}
/**
 * 9. Fiche slider
 */
function ficheSlider(objSlider) {
    if(!$(objSlider).length) { return; }

    $(objSlider).slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        arrows: false,
        dots: true
    });
}
/**
 * 10. Derniers slider
 */
function derniersSlider(objSlider) {
    if(!$(objSlider).length) { return; }

    $(objSlider).slick({
        dots: false,
        infinite: false,
        speed: 500,
        arrows: false,
        slidesToShow: 1,
        variableWidth: true
    });
}
/**
 *  11. Init Map
 */
function initMap(objMap) {
    if(!$('#' + objMap).length) { return; }
    
    if(!latMap  && !lngMap) {
		var latMap = $('#' + objMap).data('lat'),
            lngMap = $('#' + objMap).data('lng');
    }     
    var myLatlng = new google.maps.LatLng(latMap, lngMap);

    map = new google.maps.Map(document.getElementById(objMap), {
        zoom: 17,
        minZoom: 5,
        center: myLatlng,
        scrollwheel: false,
        mapTypeControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        panControl: false,
        streetViewControl: false,
        zoomControl: false,
        fullscreenControl:false
    });

    // var marker = new google.maps.Marker({
    //     position: myLatlng,
    //     title:"Hello World!"
    // });

    // To add the marker to the map, call setMap();
    // marker.setMap(map);
}
/**
 * 12. Show Buttom More page recherche-affiner
 */
function showButtonMore() {
    if(!$('.affiner-more').length) { return; }

    $(window).scroll(function(){
        var scrollTop  = $(window).scrollTop();

        if(scrollTop >= $(window).height() / 3){
            $(".affiner-more").addClass('shw bottom');
        } else{
            $(".affiner-more").removeClass('shw bottom');
        }
    });

    // change select
    $('.ipt-select').each(function(e) {
        $(this).on('change', function() {
            $(".affiner-more").addClass('shw');
        });
    });

    // change input
    $('.ipt').each(function(e) {
        $(this).on('keyup', function() {
            if( $(this).val() ) {
                $(".affiner-more").addClass('shw');
            } else {
                if(!$(".affiner-more").hasClass('bottom')) {
                    $(".affiner-more").removeClass('shw');
                }
            }
        });
    });
}
/**
 * 13. Home Agences slider
 */
function agencesSlider(objSlider) {
    if(!$(objSlider).length) { return; }

    $(objSlider).slick({
        dots: false,
        infinite: false,
        speed: 500,
        fade: false,
        arrows: true,
    });
}
/**
 * 14. Change Scroll Text
 */
function changeSrollText() {
    if(!$('.hero-scroll').length) { return;}

    var scrollText = $('.hero-scroll');
    
    $(window).scroll(function(){
        var scrollTop  = $(window).scrollTop();
        var aboutTop = $('#home-about-us').offset().top - 100;
        var searchTop = $('#home-search').offset().top - 100;
        var homeContentTop = $('.home-desktop-content').offset().top - 200;

        if(scrollTop >= homeContentTop){
            $('#hero-links').addClass('black');
        } else {
            $('#hero-links').removeClass('black');
        }

        if(scrollTop >= aboutTop){
            scrollText.addClass('fixed center');
        } else {
            scrollText.removeClass('fixed center');
        }

        if(scrollTop >= searchTop){
            scrollText.addClass('bottom');
        } else {
            scrollText.removeClass('bottom');
        }
    });
}
/**
 * 15. Scroll down block home desktop
 */
function scrollDownBlock() {
    if(!$(".hero-links").length) { return; }

    $(".hero-links li a").each(function(i) {
        $(this).on('click', function(e) {
            e.preventDefault();

            var $a_link     =   $(this),
                href        =   $a_link.attr('href');
            
            $('html, body').animate({
                scrollTop: $(href).offset().top
            },{
                queue: false,
                duration: 1000
            });
        });
    });
}
/**
 * 16. Show Filter recherche desktop
 */
function showFilterRecherche() {
    if(!$('.link_filter').length
    || !$('.recherche-filter').length) { return;}

    $('.link_filter').on('click', function(e) {
        e.preventDefault();
        var $a_filter   =   $(this),
            $body       =   $a_filter.closest('body'),
            $filter     =   $a_filter.closest('.header-recherche-search').siblings('.recherche-filter');
        
        if($filter.hasClass('shw')) {
            $filter.removeClass('shw');
            $body.removeClass('mn-filter');
        } else {
            $filter.addClass('shw');
            $body.addClass('mn-filter');
        }
    });

    // click out
    $(document).on('click', function(e) {
        if($(e.target).is('.link_filter')
        || $(e.target).is('.link_filter *')
        || $(e.target).is('.recherche-filter')
        || $(e.target).is('.recherche-filter *')) { return;}
        
        $('.recherche-filter').removeClass('shw');
        $('body').removeClass('mn-filter');
    })
}
/**
 * 17. Hide Filter recherche desktop
 */
function hideFilterRecherche() {
    if(!$('.close_filter').length) { return;}

    $('.close_filter').on('click', function(e) {
        e.preventDefault();
        var $a_close   =   $(this),
            $body       =   $a_close.closest('body'),
            $filter     =   $a_close.closest('.recherche-filter');
        
        $filter.removeClass('shw');
        $body.removeClass('mn-filter');
    });
}
/* ----------------------------------------------- */
/* ----------------------------------------------- */
/* OnLoad Page */
$(document).ready(function($){
    // 1.
    sectionSlider();
    // 2.
    agenceSlider();
    // 3.
    // showMenuRight('.menu_link');
    showMenuRight('.menu_click');
    showMenuRight('.recherche-menu');
    // 4.
    closeMenuRight();
    // 5.
    showMenuProfile();
    // 6.
    showPassword();
    // 7.
    showSearchVoice();
    // 8.
    closeSearchVoice();
    // 9.
    ficheSlider('.fiche-slider');
    ficheSlider('.technique-slider');
    ficheSlider('.hero-slider');
    // 10.
    derniersSlider('.derniers-slider');
    derniersSlider('.map-slider');
    // 11.
    initMap('map-init');
    initMap('quartier-map');
    initMap('recherche-map');
    // 12.
    showButtonMore();
    // 13.
    agencesSlider('.vos-slider');
    agencesSlider('.de-biens-slider');
    // 14.
    changeSrollText();
    // 15.
    scrollDownBlock();
    // 16.
    showFilterRecherche();
    // 17.
    hideFilterRecherche();
});
/* OnLoad Window */
$(window).on('load', function(){
    setTimeout(() => {
        $('body').removeClass('loading-blocked');
        $('.block-loading-page').hide();
    }, 2000);
});
})(jQuery);